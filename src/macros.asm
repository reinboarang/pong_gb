
Disable_LCD:	MACRO ;MUST BE IN VBLANK
				ld a, $00
				ld [LCDC_CONTROL], a
				ENDM

Enable_LCD: MACRO
			ld a, DISPLAY_FLAG|\1
			ld [LCDC_CONTROL], a
			ENDM

Copy_Data:	MACRO
			ld bc,\1
			ld hl,\2
			ld de,\3
.loop\@:	ld a,[bc]
			inc bc
			ld [hl+],a
			dec de
			xor a
			or e
			or d
			jr nz,.loop\@
			ENDM

Clear_Data:	MACRO
			ld hl,\1
			ld b,\2
			ld de,\3
.loop\@:	ld a,b
			ld [hl+],a
			dec de
			xor a
			or e
			or d
			jr nz,.loop\@
			ENDM

Wait_Vblank:	MACRO
.loop\@:		ld a, [LCDC_LY_COUNTER]
				cp $90
				jr c, .loop\@
				ENDM

Wait_Vblank_End:	MACRO
.loop\@:			ld a, [LCDC_LY_COUNTER]
					or a
					jr nz, .loop\@
					ENDM

Wait_Frames:	MACRO
				ld bc, \1
.loop\@:		Wait_Vblank
				Wait_Vblank_End
				dec bc
				xor a
				or b
				or c
				jr nz, .loop\@
				ENDM

Wait_Secs:		MACRO
				ld hl, \1
.loop\@:		Wait_Frames 60
				dec hl
				or h
				or l
				jr nz, .loop\@
				ENDM


ATTR_Y			equ	$00
ATTR_X			equ	$01
ATTR_TILE		equ $02
ATTR_FLAG		equ	$03

Set_Spr_Attr:	MACRO
				ld a, \4
				Set_Spr_Attr_Dyn \1,\2,\3
				ENDM

Set_Spr_Attr_Dyn:	MACRO
					ld [\1+(\2*4)+\3], a
					ENDM

Get_Spr_Attr:	MACRO
				ld a, [\1+(\2*4)+\3]
				ENDM
				

