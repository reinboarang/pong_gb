INCLUDE "src/header.asm"
INCLUDE "src/constants.asm"
INCLUDE "src/macros.asm"

SECTION "Hiram",HRAM

OAM_WAIT: DB

SECTION "Paddles",WRAM0[$C000]

OAM_BUFFER:	DS 160

PADDLE_1_Y:	DB
PADDLE_2_Y:	DB

BALL_X: DB
BALL_Y: DB
BALL_X_VEL: DB
BALL_Y_VEL: DB

PAD_STATE: DB

SECTION "Start", HOME[$0150]

start:
	jp main

draw:
stat:
timer:
serial:
joypad:
reti

main:
	Disable_LCD
	ld a, DEFAULT_PAL
	ld [PALETTE_BKG], a ;Set default palettes 
	ld [PALETTE_SPRITE_0], a
	ld [PALETTE_SPRITE_1], a
	Copy_Data	NORM_PADDLE, TILES_MEM_LOC_8000+($1A*$10), $10
	Copy_Data	BALL, TILES_MEM_LOC_8000+($1B*$10), $10
	Enable_LCD BKG_DISP_FLAG|TILES_LOC_8000|BKG_MAP_LOC_9800|SPRITE_DISP_FLAG
	call setup_spr
	call oam_dma
	Enable_LCD BKG_DISP_FLAG|TILES_LOC_8000|BKG_MAP_LOC_9800|SPRITE_DISP_FLAG
.loop:	
	Wait_Vblank
	call oam_dma
	call move_ball
	call read_pad
	call process_input
	call move_ai
	call update_spr
	jr .loop

setup_spr:	Clear_Data OAM_BUFFER, 0, 160
			ld a, PADDLE_INIT_Y
			ld [PADDLE_1_Y], a
			ld [PADDLE_2_Y], a
			ld a, BALL_INIT_X
			ld [BALL_X], a
			ld a, BALL_INIT_Y
			ld [BALL_Y], a
			ld a, $FF
			ld [BALL_X_VEL], a
			ld a, $01
			ld [BALL_Y_VEL], a
			;Left Paddle
			Set_Spr_Attr OAM_BUFFER, 0, ATTR_Y, PADDLE_INIT_Y
			Set_Spr_Attr OAM_BUFFER, 0, ATTR_X, PADDLE_1_X
			Set_Spr_Attr OAM_BUFFER, 0, ATTR_TILE, $1A 
			Set_Spr_Attr OAM_BUFFER, 0, ATTR_FLAG, %00000000
			Set_Spr_Attr OAM_BUFFER, 1, ATTR_Y, PADDLE_INIT_Y+8
			Set_Spr_Attr OAM_BUFFER, 1, ATTR_X, PADDLE_1_X
			Set_Spr_Attr OAM_BUFFER, 1, ATTR_TILE, $1A
			Set_Spr_Attr OAM_BUFFER, 1, ATTR_FLAG, %01000000
			;Right Paddle
			Set_Spr_Attr OAM_BUFFER, 2, ATTR_Y, PADDLE_INIT_Y
			Set_Spr_Attr OAM_BUFFER, 2, ATTR_X, PADDLE_2_X
			Set_Spr_Attr OAM_BUFFER, 2, ATTR_TILE, $1A
			Set_Spr_Attr OAM_BUFFER, 2, ATTR_FLAG, %00100000
			Set_Spr_Attr OAM_BUFFER, 3, ATTR_Y, PADDLE_INIT_Y+8
			Set_Spr_Attr OAM_BUFFER, 3, ATTR_X, PADDLE_2_X
			Set_Spr_Attr OAM_BUFFER, 3, ATTR_TILE, $1A
			Set_Spr_Attr OAM_BUFFER, 3, ATTR_FLAG, %01100000
			;Ball
			Set_Spr_Attr OAM_BUFFER, 4, ATTR_Y, BALL_INIT_Y
			Set_Spr_Attr OAM_BUFFER, 4, ATTR_X, BALL_INIT_X
			Set_Spr_Attr OAM_BUFFER, 4, ATTR_TILE, $1B
			Set_Spr_Attr OAM_BUFFER, 4, ATTR_FLAG, %00000000
			ret	

update_spr:	ld a, [PADDLE_1_Y]
			add a, $10
			Set_Spr_Attr_Dyn OAM_BUFFER, 0, ATTR_Y
			add a, $08
			Set_Spr_Attr_Dyn OAM_BUFFER, 1, ATTR_Y
			ld a, [PADDLE_2_Y]
			add a, $10
			Set_Spr_Attr_Dyn OAM_BUFFER, 2, ATTR_Y
			add a, $08
			Set_Spr_Attr_Dyn OAM_BUFFER, 3, ATTR_Y
			ld a, [BALL_X]
			Set_Spr_Attr_Dyn OAM_BUFFER, 4, ATTR_X
			ld a, [BALL_Y]
			add a, $10
			Set_Spr_Attr_Dyn OAM_BUFFER, 4, ATTR_Y
			ret

move_ball:	ld a, [BALL_X]
			or a
			jr nz, .skip2	;skip2 if BALL_X != 0

			ld a, BALL_INIT_X
			ld [BALL_X], a
			ld a, BALL_INIT_Y
			ld [BALL_Y], a
			ld a, $FF
			ld [BALL_X_VEL], a
			ld a, $01
			ld [BALL_Y_VEL], a	;RESET BALL

.skip2:		ld a, [BALL_X]	;skip3 if BALL_X != $98
			cp $A8
			jr nz, .skip3

			ld a, BALL_INIT_X
			ld [BALL_X], a
			ld a, BALL_INIT_Y
			ld [BALL_Y], a
			ld a, $FF
			ld [BALL_X_VEL], a
			ld a, $01
			ld [BALL_Y_VEL], a	;RESET BALL
	
.skip3		ld a, [BALL_X_VEL]	;Update BALL_X
			ld b, a
			ld a, [BALL_X]
			add a, b
			ld [BALL_X], a
			ld a, b
			cp $FF			;skip if BALL_X_VEL != $FF
			jr nz, .skip1

			ld a, [BALL_X]
			ld b, a
			ld a, PADDLE_1_X
			add a, $06
			cp b
			jr nz, .skip1 	;skip if BALL_X != PADDLE_1_X+6
			
			ld a, [BALL_Y]
			ld c, a
			add a, $06
			ld d, a
			ld a, [PADDLE_1_Y]
			ld b, a
			ld a, d
			cp b			;skip if BALL_Y+8 < PADDLE_1_Y 
			jr c, .skip1
			
			ld a, b
			add a, $0D
			cp c			;skip if BALL_Y > PADDLE_1_Y+16
			jr c, .skip1

			ld a, $01
			ld [BALL_X_VEL], a

	
.skip1:		ld a, [BALL_X_VEL]
			cp $01
			jr nz, .y_vel	;skip if BALL_X_VEL != $01
			
			ld a, [BALL_X]
			add a, $06
			ld b, a
			ld a, PADDLE_2_X
			cp b
			jr nz, .y_vel	;skip if BALL_X+6 != PADDLE_2_X
			
			ld a, [BALL_Y]
			ld e, a
			ld a, [PADDLE_2_Y]
			ld d, a
			add a, $0D
			cp e
			jr c, .y_vel	;skip if BALL_Y > PADDLE_2_Y+$0D

			ld a, e
			add a, $06
			cp d
			jr c, .y_vel		;skip if BALL_Y+6 < PADDLE_2_Y

			ld a, $FF
			ld [BALL_X_VEL], a

.y_vel:		ld a, [BALL_Y]
			ld b, a
			ld a, [BALL_Y_VEL]
			add a, b

			ld [BALL_Y], a
			ld a, [BALL_Y]
			cp $FE
			jr nz, .skip4
			ld a, $01
			ld [BALL_Y_VEL], a
			
.skip4		ld a, [BALL_Y]
			cp $8A
			jr nz, .skip_end
			ld a, $FF
			ld [BALL_Y_VEL], a
			
.skip_end:	ret

read_pad:	ld a, PAD_PORT_DPAD
			ld [JOYPAD_REGISTER], a
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			cpl
			and $0F
			sla a
			sla a
			sla a
			sla a
			ld b, a
			ld a, PAD_PORT_BUTTONS
			ld [JOYPAD_REGISTER], a
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			ld a, [JOYPAD_REGISTER]
			cpl
			and $0F
			or b
			ld [PAD_STATE], a
			ret

process_input:	ld a, [PAD_STATE]
				ld b, a
				bit DPAD_UP,a
				jr z, .next_button1
				ld a, [PADDLE_1_Y]
				cp $00
				jr z, .next_button1
				sub a, PADDLE_INC
				ld [PADDLE_1_Y], a
.next_button1:	ld a, b
				bit DPAD_DOWN, a
				jr z, .next_button2
				ld a, [PADDLE_1_Y]
				cp $90-$10
				jr z, .next_button2
				add a, PADDLE_INC
				ld [PADDLE_1_Y], a
.next_button2:	ret

move_ai:		ld a, [PADDLE_2_Y]
				add a, $08			;B = Middle of Paddle 2 Y coord
				ld b, a
				
				ld a, [BALL_Y]
				add a, $04			;A = Middle of Ball Y coord
				
				cp b
				jr z, .skip
				jr nc, .move_down
.move_up:		ld a, [PADDLE_2_Y]
				add a, $FF
				ld [PADDLE_2_Y], a
				jr .skip
.move_down:		ld a, [PADDLE_2_Y]
				add a, $01
				ld [PADDLE_2_Y], a
.skip:			ret
				


oam_dma:	Copy_Data oam_sub, OAM_WAIT, oam_end-oam_sub
			call OAM_WAIT
			ret 

oam_sub:	ld a, OAM_BUFFER/$100
			ld [DMA_REGISTER], a
			ld a, $28
.loop\@:	dec a
			jr nz, .loop\@
			ret
oam_end:	nop


;; ======= GRAPHICS DATA =======

NORM_PADDLE: 	INCBIN "data/sprites/normal_paddle.bin"
BALL: 			INCBIN "data/sprites/ball.bin"

