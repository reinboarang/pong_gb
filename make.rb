sprites = Dir.entries('./data/sprites')[2..-1]
tiles = Dir.entries('./data/tiles')[2..-1]

puts "Assembling..."
raise "Assembly failed!" unless system("rgbasm -o build/main.o src/main.asm") == true
puts "Linking..."
raise "Linking failed!" unless system("rgblink -o main.gb build/main.o") == true
puts "Fixing ROM..."
raise "ROM Patching failed!" unless system("rgbfix main.gb") == true
puts "Done!"

