# Targets
TARGET = main.gb
SPRITES = $(wildcard data/sprites/*.png)
SPRITES_BIN = $(SPRITES:.png=.bin)

# Flags
RGBASM_F = 
RGBLINK_F =
RGBFIX_F = -v
RGBGFX_SPR_F = -fv

all: $(SPRITES_BIN) $(TARGET)

.PHONY: sprites
sprites: $(SPRITES_BIN)

$(SPRITES_BIN): $(SPRITES)
	$(foreach spr,$(SPRITES),$(shell rgbgfx $(RGBGFX_SPR_F) -o $(spr:.png=.bin) $(spr)))
	

$(TARGET): build/main.o
	rgblink -o main.gb -n main.sym build/main.o
	rgbfix $(RGBFIX_F) main.gb

build/main.o: src/main.asm
	rgbasm -o build/main.o src/main.asm

run: $(SPRITES_BIN) $(TARGET)
	wine ~/Downloads/bgb.exe main.gb

.PHONY: justrun
justrun:
	wine ~/Downloads/bgb.exe main.gb

.PHONY: clean
clean:
	echo $(SPRITES_BIN)
	rm main.gb build/main.o
	rm $(SPRITES_BIN)

